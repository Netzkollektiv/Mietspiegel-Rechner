
// re-calculate after changes to input

document.addEventListener("DOMContentLoaded", function()
{
    document.querySelectorAll("form input").forEach(input => {
        input.addEventListener("change", (event) => runCalculation());
    });
    document.querySelectorAll("form select").forEach(input => {
        input.addEventListener("change", (event) => runCalculation());
    });
    document.querySelectorAll("form input[type=number]").forEach(input => {
        input.addEventListener("keyup", (event) => runCalculation());
    });
    // make checkboxes clickable from within the whole row
    for (row of document.getElementsByClassName("row-input")) {
        row.addEventListener("click", (event) => {
            let inputs = event.target.getElementsByTagName("input")
            if (inputs.length == 1) {
            inputs[0].click();
            }
        });
    }
    runCalculation();
});

function runCalculation()
{
    let result = undefined;
	let resultTotal = undefined;
    try {
        let flatSize = Number(document.getElementById("flatSize").value);
        let score = getAllScores(flatSize);
        result = referencePriceFor(score, flatSize).toFixed(2);
		resultTotal = (result * flatSize).toFixed(0);
		console.log(result);
    } catch (exception) {
        console.log(exception);
    }
    if (result == undefined || result == 0) {
        document.getElementById("referencePriceTotal").value = "-";
		document.getElementById("referencePrice").value = "-";

    } else {
        document.getElementById("referencePriceTotal").value = resultTotal;
		document.getElementById("referencePrice").value = result;
    }
}

/**
 * The following functions start with a "get". Those are used to handle the user input.
 * The functions following those, end with "for" and are the functions agnostic to the user input.
 *
 */


function getAllScores(flatSize) {
    let referencePricePerMeter = pricePerMeterReferenceFor(flatSize);
    document.getElementById("basic-price").value = Number(referencePricePerMeter.toFixed(2));

    let constructionScore = getConstructionScore();
    let sanitaryScore = getSanitaryScore();
    let amenityScore = getAmenityScore();
    let modernisationScore = getModernisationScore();
    let residentialScore = getResidentialScore();
    let score = constructionScore + modernisationScore +
                amenityScore + sanitaryScore + residentialScore;


    document.querySelector("#sectionConstruction .section-score").value   = constructionScore;
    document.querySelector("#sectionSanitary .section-score").value       = sanitaryScore;
    document.querySelector("#sectionAmenities .section-score").value      = amenityScore;
    document.querySelector("#sectionModernisation .section-score").value  = modernisationScore;
    document.querySelector("#sectionResidential .section-score").value    = residentialScore;

    document.querySelector("#sectionConstruction .section-price").value  = (referencePricePerMeter * constructionScore / 100).toFixed(2);
    document.querySelector("#sectionSanitary .section-price").value      = (referencePricePerMeter * sanitaryScore / 100).toFixed(2);
    document.querySelector("#sectionAmenities .section-price").value     = (referencePricePerMeter * amenityScore / 100).toFixed(2);
    document.querySelector("#sectionModernisation .section-price").value = (referencePricePerMeter * modernisationScore / 100).toFixed(2);
    document.querySelector("#sectionResidential .section-price").value   = (referencePricePerMeter * residentialScore / 100).toFixed(2);
    
    document.querySelector("#sectionConstruction .section-price-total").value   = (flatSize * referencePricePerMeter * constructionScore / 100).toFixed(2);
    document.querySelector("#sectionSanitary .section-price-total").value       = (flatSize * referencePricePerMeter * sanitaryScore / 100).toFixed(2);
    document.querySelector("#sectionAmenities .section-price-total").value      = (flatSize * referencePricePerMeter * amenityScore / 100).toFixed(2);
    document.querySelector("#sectionModernisation .section-price-total").value  = (flatSize * referencePricePerMeter * modernisationScore / 100).toFixed(2);
    document.querySelector("#sectionResidential .section-price-total").value    = (flatSize * referencePricePerMeter * residentialScore / 100).toFixed(2);

    return score
}

function getConstructionScore() {
    return Number(document.getElementById("yearConstruction").value);
}

function getAmenityScore()
{
    // the following html input elements are checkboxes with "checked-value" or "unchecked-value" attributes
    // those are added to the score sum if the box is checked or unchecked, respectively.
    amenities = ["amenitiesBadBalcony",
                 "amenitiesElevator",
                 "amenitiesRooftopWintergarden",
                 "amenitiesKitchen",
                 "amenitiesFlooring",
                 "amenitiesAccessibility"
                ];
    let score = 0;
    for (amenity of amenities)
    {
        let amenityCheckbox = document.getElementById(amenity);
        if (amenityCheckbox == undefined)
        {
            throw "amenity " + amenity + "not found";
        }
        if (amenityCheckbox.checked)
        {
            score += Number(amenityCheckbox.getAttribute("checked-value"))
        } else {
            score += Number(amenityCheckbox.getAttribute("unchecked-value"))

        }
    }
    return score;

}

function getSanitaryScore() {
    // the following html input elements are checkboxes with "checked-value" or "unchecked-value" attributes
    // those are added to the score sum if the box is checked or unchecked, respectively.
    // from the sanitaryScore a score is caluclated for the total-score as explained in the pdf doc.
    sanitaryFeatures = [ "sanitaryTiling",
                         "sanitaryWindow",
                         "sanitarySecondSink",
                         "sanitaryTowelRadiator",
                         "sanitaryHeatedFloor",
                         "sanitaryBathtubShower",
                         "sanitaryShowerAccessibility"
                        ]
    let sanitaryScore = 0;
    for (sanitaryFeature of sanitaryFeatures)
    {
        let sanitaryCheckbox = document.getElementById(sanitaryFeature);
        if (sanitaryCheckbox.checked) {
            // if attribute is not present, 0 is added
            sanitaryScore += Number(sanitaryCheckbox.getAttribute("checked-value"));
        } else {
            sanitaryScore += Number(sanitaryCheckbox.getAttribute("unchecked-value"));
        }
    }
    // here the sanitary score gets translated to the "global" score which is used to add to the others.
    if (sanitaryScore == -2 || sanitaryScore == -1) {
        return -4;
    } else if (sanitaryScore == 0 || sanitaryScore == 1) {
        return 0;
    } else if (sanitaryScore >= 2) {
        return 5;
    }
}


function referencePriceFor(score, flatSize) {
    let referencePricePerMeter = pricePerMeterReferenceFor(flatSize);

    /* return 0 if no valid flatSize was given */
    if (referencePricePerMeter > 0)
    {
        let c = referencePricePerMeter * score / 100;
        let d = referencePricePerMeter + c;
        
        return d;
    }
    else
    {
        return 0;
    }
}

function getResidentialScore() {
    // return residentialScoreFor(
    //         document.querySelector("#residentialArea").value);

    return Number(document.querySelector("#residentialArea").value);
}

// function residentialScoreFor(residenceCode) {
//     switch (residenceCode) {
//         case "1":
//             return -9;
//         case "2":
//             return 0;
//         case "3":
//             return 7;
//         default:
//             throw "no such residence code"
//     }
// }

function getModernisationScore()
{
    modernisationFeatures = [ "modernisationBasement",
                              "modernisationExteriorWall",
                              "modernisationRoof",
                              "modernisationWindows",
                              "modernisationElectrics",
                              "modernisationSanitary"
                            ]

    let modernisationScore = Number(document.getElementById("yearRefurbishment").value);

    /* only take further modernisations into account if no comprehensive refurbishment was done after 2010 */
    if (modernisationScore == 0 || modernisationScore == 4)
    {
        for (modernisationFeature of modernisationFeatures)
        {
            let modernisationCheckbox = document.getElementById(modernisationFeature);
            if (modernisationCheckbox.checked) {
                // if attribute is not present, 0 is added
                modernisationScore += 1;
            } else {
                /* do nothing */
            }
        }
    }

    return modernisationScore;
}


function pricePerMeterReferenceFor(area) {
    /* Mietspiegel only applies for households between 20 and 150 square meters */
    if (area > 20 && area < 150) {
        return pricePerMeterReferenceTable[area];
    } else {
        return 0;
    }
}

let pricePerMeterReferenceTable =
{
20: 7.53,
21: 7.53,
22: 7.21,
23: 7.21,
24: 6.94,
25: 6.94,
26: 6.73,
27: 6.73,
28: 6.54,
29: 6.54,
30: 6.39,
31: 6.39,
32: 6.27,
33: 6.27,
34: 6.16,
35: 6.16,
36: 6.07,
37: 6.07,
38: 6.00,
39: 6.00,
40: 5.93,
41: 5.93,
42: 5.88,
43: 5.88,
44: 5.88,
45: 5.88,
46: 5.80,
47: 5.80,
48: 5.77,
49: 5.77,
50: 5.74,
51: 5.74,
52: 5.72,
53: 5.72,
54: 5.71,
55: 5.71,
56: 5.70,
57: 5.70,
58: 5.70,
59: 5.70,
60: 5.69,
61: 5.69,
62: 5.69,
63: 5.69,
64: 5.70,
65: 5.70,
66: 5.70,
67: 5.70,
68: 5.71,
69: 5.71,
70: 5.72,
71: 5.72,
72: 5.73,
73: 5.73,
74: 5.75,
75: 5.75,
76: 5.76,
77: 5.76,
78: 5.78,
79: 5.78,
80: 5.80,
81: 5.80,
82: 5.82,
83: 5.82,
84: 5.84,
85: 5.84,
86: 5.86,
87: 5.86,
88: 5.89,
89: 5.89,
90: 5.91,
91: 5.91,
92: 5.94,
93: 5.94,
94: 5.96,
95: 5.96,
96: 5.99,
97: 5.99,
98: 6.02,
99: 6.02,
100: 6.05,
101: 6.05,
102: 6.08,
103: 6.08,
104: 6.11,
105: 6.11,
106: 6.14,
107: 6.14,
108: 6.17,
109: 6.17,
110: 6.20,
111: 6.20,
112: 6.23,
113: 6.23,
114: 6.26,
115: 6.26,
116: 6.30,
117: 6.30,
118: 6.33,
119: 6.33,
120: 6.36,
121: 6.36,
122: 6.40,
123: 6.40,
124: 6.43,
125: 6.43,
126: 6.47,
127: 6.47,
128: 6.50,
129: 6.50,
130: 6.54,
131: 6.54,
132: 6.57,
133: 6.57,
134: 6.61,
135: 6.61,
136: 6.65,
137: 6.65,
138: 6.68,
139: 6.68,
140: 6.72,
141: 6.72,
142: 6.76,
143: 6.76,
144: 6.79,
145: 6.79,
146: 6.83,
147: 6.83,
148: 6.88,
149: 6.88,
150: 6.88
}
