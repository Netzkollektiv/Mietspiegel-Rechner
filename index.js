function getDocHeight(doc) {
    doc = doc || document;
    // stackoverflow.com/questions/1145850/
    var body = doc.body, html = doc.documentElement;
    var height = Math.max( body.scrollHeight, body.offsetHeight,
        html.clientHeight, html.scrollHeight, html.offsetHeight );
    return height;
}

function setIframeHeight(id) {
    var iframe = document.getElementById(id);
    var doc = iframe.contentDocument? iframe.contentDocument:
        iframe.contentWindow.document;
    iframe.style.visibility = 'hidden';
    iframe.style.height = "10px"; // reset to minimal height ...
    // IE opt. for bing/msn needs a bit added or scrollbar appears
    iframe.style.height = getDocHeight( doc ) + 4 + "px";
    iframe.style.visibility = 'visible';
}
