# Mietspiegel Rechner
*Rechner für die individuelle "ortsübliche Vergleichsmiete" analog zum Entwurf des neuen Mietspiegels für die Stadt Halle (Saale) aus dem Jahr 2024.*

*Lizenz: GPLv3*

Web: https://mietspiegel-halle.de

--- 

#### Was macht der Mietspiegel-Rechner?

Halle soll einen neuen Mietspiegel bekommen. Doch, was bedeutet das für Dich persönlich?  
Welche "ortsübliche Vergleichsmiete" gilt für Deine Wohnung in Zukunft?  

Um die Auswirkungen des Mietspiegels einfach zugänglich zu verdeutlichen, kannst du unseren Mietspiegel-Rechner verwenden. Einfach so genau wie möglich die Daten zu deiner Wohnung eintragen und das Ergebnis ermitteln.

---

#### Wie nutze ich den Mietspiegel-Rechner auf meiner Website?

```html
<iframe
  src="https://mietspiegel-halle.de/rechner"
  width="100%"
  height="720px"
>
</iframe>
```

Einfach diesen Code auf einer Seite deiner Website einfügen. Wenn du zum Beispiel Wordpress nutzt, kannst du dies mit dem Block "Individuelles HTML" tun.
